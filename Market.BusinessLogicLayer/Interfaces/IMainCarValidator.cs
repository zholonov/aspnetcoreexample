﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.BusinessLogicLayer.Interfaces
{
    public interface IMainCarValidator
    {
        bool IsValid(int releaseDate);
    }
}
