﻿using Market.BusinessLogicLayer.Interfaces;

namespace Market.BusinessLogicLayer
{
    public class MainCarValidator : IMainCarValidator
    {
        public bool IsValid(int releaseDate)
        {
            return releaseDate > 0 && releaseDate <= DateTime.Now.Year;
        }
    }
}