﻿using CarMarket.Data;
using Market.BusinessLogicLayer;
using Market.BusinessLogicLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<CarMarketDBContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("CarMarketDBContext") ?? throw new InvalidOperationException("Connection string 'CarMarketDBContext' not found.")));

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddScoped<IMainCarValidator, MainCarValidator>();
builder.Services.TryAddScoped<IMainCarValidator, MainCarValidator>();

//builder.Services.AddSingleton<IMainCarValidator, MainCarValidator>();
//builder.Services.AddTransient<IMainCarValidator, MainCarValidator>();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    SeedDataForCars.InitializeCars(services);
}

    // Configure the HTTP request pipeline.
    if (!app.Environment.IsDevelopment())
    {
        app.UseExceptionHandler("/Home/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Cars}/{action=Index}/{id?}");

app.Run();
