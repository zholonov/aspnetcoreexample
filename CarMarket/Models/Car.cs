﻿namespace CarMarket.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public Engine Engine { get; set; }
        public int ReleaseYear { get; set; }
    }

    public enum Engine
    {
        Diesel,
        Gazolin,
        Gas
    }
}
