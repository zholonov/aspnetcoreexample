﻿using Microsoft.EntityFrameworkCore;

namespace CarMarket.Data
{
    public static class SeedDataForCars
    {
        public static void InitializeCars(IServiceProvider serviceProvider)
        {
            using (var dbContext = new CarMarketDBContext(serviceProvider.GetRequiredService<DbContextOptions<CarMarketDBContext>>()))
            {
                if (!dbContext.Car.Any())
                {
                    dbContext.Car.Add(new Models.Car
                    {
                        Model = "Mercedes",
                        ReleaseYear = 2023,
                        Color = "Red",
                        Engine = Models.Engine.Gazolin
                    });

                    dbContext.Car.AddRange(
                        new Models.Car
                        {
                            Model = "BMW",
                            ReleaseYear = 2023,
                            Color = "Yellow",
                            Engine = Models.Engine.Gazolin
                        },
                        new Models.Car
                        {
                            Model = "Hundai",
                            ReleaseYear = 2023,
                            Color = "Green",
                            Engine = Models.Engine.Gas
                        });

                    dbContext.SaveChanges();
                }
            }
        }
    }
}
