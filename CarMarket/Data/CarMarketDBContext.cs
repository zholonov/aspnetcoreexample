﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CarMarket.Models;

namespace CarMarket.Data
{
    public class CarMarketDBContext : DbContext
    {
        public CarMarketDBContext (DbContextOptions<CarMarketDBContext> options)
            : base(options)
        {
        }

        public DbSet<CarMarket.Models.Car> Car { get; set; } = default!;
    }
}
